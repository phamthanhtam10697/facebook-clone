import firebase from "firebase";

const firebaseConfig = {
  apiKey: "AIzaSyDc75C2GGbergxVbarrEWN44Au_EKTih_A",
  authDomain: "fb-clone-6b9b6.firebaseapp.com",
  databaseURL: "https://fb-clone-6b9b6.firebaseio.com",
  projectId: "fb-clone-6b9b6",
  storageBucket: "fb-clone-6b9b6.appspot.com",
  messagingSenderId: "631243654350",
  appId: "1:631243654350:web:e0441c73089a72d1817c9f",
};

const firebaseApp = firebase.initializeApp(firebaseConfig)
const db = firebaseApp.firestore()

const auth = firebase.auth()
const provider = new firebase.auth.GoogleAuthProvider()

export {auth, provider}
export default db