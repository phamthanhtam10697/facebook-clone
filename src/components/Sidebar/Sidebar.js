import React from 'react'
import SidebarRow from './SidebarRow'
import LocalHospitalIcon from '@material-ui/icons/LocalHospital';
import PeopleIcon from '@material-ui/icons/People';
import EmojiEmotionsOutlinedIcon from '@material-ui/icons/EmojiEmotionsOutlined';
import ChatIcon from '@material-ui/icons/Chat';
import StorefrontIcon from '@material-ui/icons/Storefront';
import VideoLibraryIcon from '@material-ui/icons/VideoLibrary';
import ExpandMoreOutlinedIcon from '@material-ui/icons/ExpandMoreOutlined';
import { useStateValue } from '../../store/StateProvider';
function Sidebar() {
    const [{user}, dispatch] = useStateValue()
    return (
        <div className='sidebar'>
            <SidebarRow src={user.photoURL} title={user.displayName} />
            <SidebarRow title='Covid 19 Infomation Center' Icon={LocalHospitalIcon} />
            <SidebarRow title='Pages' Icon={EmojiEmotionsOutlinedIcon} />
            <SidebarRow title='Friends' Icon={PeopleIcon} />
            <SidebarRow title='Messenger' Icon={ChatIcon} />
            <SidebarRow title='MarketPlace' Icon={StorefrontIcon} />
            <SidebarRow title='Videos' Icon={VideoLibraryIcon} />
            <SidebarRow title='More' Icon={ExpandMoreOutlinedIcon} />
        </div>
    )
}

export default Sidebar
