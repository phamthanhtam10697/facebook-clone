import React, { useState, useEffect } from "react";
import StoryReel from "./StoryReel";
import MessageSender from "./MessageSender";
import Post from "./Post";
import db from "../../firebase";

function Feed() {
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    db.collection("posts")
      .orderBy("timestaps", "desc")
      .onSnapshot((snapshot) => {
        setPosts(
          snapshot.docs.map((doc) => ({ id: doc.id, data: doc.data() }))
        );
      });
  }, []);

  return (
    <div className="feed">
      {/* story */}
      <StoryReel />
      {/* Message Sender */}
      <MessageSender />
      {/* Post */}
      {posts.map((post) => (
        <Post
          key={post.id}
          image={post.data.image}
          username={post.data.username}
          timestaps={post.data.timestaps}
          profilePic={post.data.profilePic}
          message={post.data.message}
        />
      ))}
    </div>
  );
}

export default Feed;
