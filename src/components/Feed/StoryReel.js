import React from 'react'
import Story from './Story'
import './Feed.css'

function StoryReel() {
    return (
        <div className='storyReel'>
            <Story title='Frankie' profileSrc='https://img.icons8.com/bubbles/2x/user-male.png' image='https://picsum.photos/400/400' />
            <Story title='Bruce' profileSrc='https://img.icons8.com/bubbles/2x/user-male.png' image='https://picsum.photos/400/400' />
            <Story title='Frifox' profileSrc='https://img.icons8.com/bubbles/2x/user-male.png' image='https://picsum.photos/400/400' />
            <Story title='Battomal' profileSrc='https://img.icons8.com/bubbles/2x/user-male.png' image='https://picsum.photos/400/400' />
            <Story title='Thigo' profileSrc='https://img.icons8.com/bubbles/2x/user-male.png' image='https://picsum.photos/400/400' />
            <Story title='Silva' profileSrc='https://img.icons8.com/bubbles/2x/user-male.png' image='https://picsum.photos/400/400' />
        </div>
    )
}

export default StoryReel
