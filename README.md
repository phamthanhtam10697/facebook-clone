# Facebook Clone 
Build Facebook Clone using ReactJS

## Demo
[Link](https://fb-clone-6b9b6.web.app/)

## Tech use in facebook app
- react + react hooks
- react context API (redux pattern)
- Material UI
- Firebase's firestore realtime DB
- Firebase Hosting
- Firebase Google Authentication

## Installation
Use the package manager npm to install dependencies.
> npm i

## Run
> npm start


